bash-speed-tests
================

## About

This is a collection of simple scripts to vivisect the ~/.bashrc file and to measure its startup time by parts. Depends on [`sepa`](https://bitbucket.org/dsjkvf/sepa/) and [`hyperfine`](https://github.com/sharkdp/hyperfine).
