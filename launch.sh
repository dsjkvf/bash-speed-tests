#!/bin/bash

help() {
    echo "USAGE:"
    echo "      ./launch.sh prepare     # to copy .bashrc here and split"
    echo "      ./launch.sh test        # to test"
}

if ! [ $# -eq 1 ]
then
    help
elif [ $1 = prepare ]
then
    cp $(readlink $HOME/.bashrc) ./bashrc
    sepa bashrc '^# [A-Z][a-z]'
elif [ $1 = test ]
then
    hyperfine -w 10 -r 20 --show-output "bash --rcfile launch -i"
elif [ $1 = clean ]
then
    rm log bashrc *.part
else
    help
fi
