cat $1 | awk -v z=0 -v k=0 -v t=0 '{z+=1; k+=$2; if (z==30) {printf "%75s %20.0f\n" , $1 , k*100/3; t+=k; k=0; z=0}} END {printf "%75s %20.0f\n\n", "TOTAL:", t*100/3}'
